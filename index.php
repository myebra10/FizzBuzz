<!DOCTYPE html>
<html lang="es">
    <title>Prueba de PHP</title>
    <?php
        $num_min = 1;
	    $num_max = 100;
	    $value = "";
	    while($num_min <= $num_max){
		    if ($num_min % 3 == 0 && $num_min % 5 == 0){
			    $value = "FizzBuzz";
		    }
		    else{
			    if ($num_min % 3 == 0){
				    $value = "Fizz";
			    }
			    elseif ($num_min % 5 == 0){
				    $value = "Buzz";
			    }
			    else{
				    $value = $num_min;
			    }
		    }
		    echo "<p>" . $value . "</p>";
		    $num_min++;
	    }
    ?>
 </head>
 <body>
 </body>
</html>